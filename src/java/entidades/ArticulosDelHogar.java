package entidades;

public class ArticulosDelHogar {

    private String nombre;
    private String marca;
    private String tipo;
    private double precio;
    private String tamanio;
    private String color;
    private String estadoCompra = "Sin comprar";

    void comprar(int cantidad) {
        if (cantidad > 0) {
            //caso verdadero
            this.estadoCompra = "Comprado";
            System.out.println("Usted compró el producto: "
                    + nombre + " " + cantidad + " unidades.");

            System.out.println("El costo es de: " + this.precio * cantidad);
        } else {
            //caso falso
            System.out.println("Ni loco podes comprar cosas inexistentes, chabón!");
        }

    }

    public ArticulosDelHogar(String nombre, String marca, String tipo,
            double precio, String tamanio, String color) {
        this.nombre = nombre;
        this.marca = marca;
        this.tipo = tipo;
        this.precio = precio;
        this.tamanio = tamanio;
        this.color = color;
    }

    @Override
    public String toString() {
        return "ArticulosDelHogar{" + "nombre:" + nombre + ", marca:" + marca + ", "
                + "tipo:" + tipo + ", precio:" + precio + ", tamanio:"
                + tamanio + ", color:" + color + ", estadoCompra:" + estadoCompra + '}';
    }

}
